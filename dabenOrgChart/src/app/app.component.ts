import {Component} from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	title = 'dabenOrgChart';
	// ds = {
	// 	id: '1',
	// 	name: 'Horse 1',
	// 	children: [
	// 		{
	// 			id: '2',
	// 			name: 'Horse 21',
	// 			children: [
	// 				{id: '4', name: 'Horse 31'},
	// 				{
	// 					id: '5',
	// 					name: 'Horse 32',
	// 				},
	// 			],
	// 		},
	// 		{
	// 			id: '3',
	// 			name: 'Horse 22',
	// 			children: [
	// 				{id: '4', name: 'Horse 31'},
	// 				{
	// 					id: '5',
	// 					name: 'Horse 32',
	// 				},
	// 			],
	// 		},
	// 	],
	// };
	ds = {
		id: '1',
		name: 'Lao Lao',
		title: 'Horse 1',
		img: 'assets/images/horse5.jpg',
		children: [
			{
				id: '2',
				name: 'Bo Miao',
				title: 'Horse 21',
				img: 'assets/images/horse2.jpg',
				children: [
					{id: '3', name: 'Tie Hua', title: 'Horse 31', img: 'assets/images/horse3.jpg'},
					{
						id: '4',
						name: 'Hei Hei',
						title: 'Horse 32',
						img: 'assets/images/horse5.jpg',
						children: [
							{
								id: '5',
								name: 'Tie Hua',
								title: 'Horse 31',
								img: 'assets/images/horse3.jpg',
							},
						],
					},
				],
			},
			{
				id: '6',
				name: 'Su Miao',
				title: 'Horse 22',
				img: 'assets/images/horse7.jpg',

				children: [
					{id: '7', name: 'Tie Hua', title: 'Horse 31', img: 'assets/images/horse3.jpg'},
					{
						id: '8',
						name: 'Hei Hei',
						title: 'Horse 32',
						img: 'assets/images/horse5.jpg',
					},
				],
			},
			{
				id: '9',
				name: 'Bo Miao',
				title: 'Horse 21',
				img: 'assets/images/horse6.jpg',
				children: [
					{id: '10', name: 'Tie Hua', title: 'Horse 31', img: 'assets/images/horse3.jpg'},
					{
						id: '11',
						name: 'Hei Hei',
						title: 'Horse 32',
						img: 'assets/images/horse2.jpg',
						children: [
							{
								id: '12',
								name: 'Tie Hua',
								title: 'Horse 31',
								img: 'assets/images/horse3.jpg',
							},
						],
					},
				],
			},
		],
	};
	nodeHeadingProperty = 'name';
	// nodeContentProperty = 'title';
	directionProperty = 'l2r';
	ngOnInit() {}

	selectNode(nodeData: any) {
		alert("Hi All. I'm " + nodeData.name + ". I'm a " + nodeData.title + '.');
	}
}
