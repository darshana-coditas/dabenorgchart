import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {OrgchartModule} from '@dabeng/ng-orgchart';

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, OrgchartModule],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
